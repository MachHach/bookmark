# Bookmark

Personal awesome list of bookmarked links.

## General

Knowledge

* **Message format**: XML, JSON, YAML

Web

* **Full-stack**: Web server, back-end, front-end, caching, infrastructure monitoring, virtualization, search server
* **Front-end terms**: HTML, HTML5, XHTML, CSS, CSS3, media query, DHTML
* **CSS pre-processors**: SASS, Less.js
* **JavaScript languages**: ECMAScript 6 (ES6 / ES2015), TypeScript
* **Front-end framework**: Twitter Bootstrap, Materialize
* **Single-page-app frameworks**: Angular, React, Vue, Ember, Knockout

Tools

* **Text editors**: Vim, GNU Emacs, Notepad++, Atom, Sublime, VSCode
* **Version control softwares**: Git, Mercurial, SVN (Apache Subversion), Perforce
* **Version control hosted services**: GitHub, Altassian Bitbucket, GitLab
* **CI servers**: Jenkins, Altassian Bamboo, Gitlab, TeamCity, Travis, CircleCI

Infrastructure

* **Persistence**:
    - Relational databases: SQLite, MySQL, MariaDB, PostgreSQL,
    - NoSQL databases: MongoDB
    - In-memory: Redis
    - Others: Apache Cassandra
* **Linux distros**:
    - Debian based: Ubuntu, Raspbian
    - Red Hat based: CentOS
    - Others: Kali, ArchLinux, Alpine
* **Proxy web servers**: Apache httpd, Nginx, HAProxy
* **Search servers**: Elasticsearch, Apache Solr, Sphinx
* **Virtualization**:
    - VMs: Oracle VirtualBox, VMWare, Microsoft Hyper-V
    - Containers: Docker, OCI
* **VM tools**: Vagrant
* **PaaS cloud**: Heroku
* **IaaS cloud**: Amazon Web Services, Microsoft Azure, Google Cloud Platform, IBM Cloud
* **VPS cloud**: DigitalOcean, Linode

Misc

* **Mobile app development**:
    - Android: Java / Kotlin
    - iOS: Swift / Objective-C
    - Hybrid: React Native / Ionic / Apache Cordova / Microsoft Xamarin
* **IoT development**:
    - Raspberry Pi
    - Arduino: C / C++
* **Game development**
    - Unity: UnityScript
    - Unreal Engine
    - LibGDX: Java
    - JMonkey: Java

## Linux

### Linux daemons

* [Systemd's `systemctl` vs SysVinit's `service`](https://askubuntu.com/questions/911525/difference-between-systemctl-init-d-and-service)
* [How to use Systemd `systemctl`](https://www.digitalocean.com/community/tutorials/how-to-use-systemctl-to-manage-systemd-services-and-units)

## Node.js

### Node.js packages

* [`async` - Asynchronous function helper](https://www.npmjs.com/package/async)
* [`body-parser` - Middleware to parse HTTP body](https://www.npmjs.com/package/body-parser)
* [`express` - Simple web framework](https://www.npmjs.com/package/express)
* [`ejs` - HTML templating library](https://www.npmjs.com/package/ejs)
* [`bootstrap` - Twitter Bootstrap CSS JavaScript framework](https://www.npmjs.com/package/bootstrap)
* [`jquery` - JavaScript high-level wrapper functions](https://www.npmjs.com/package/jquery)
* [`sqlite` - SQLite database client](https://www.npmjs.com/package/sqlite)
* [`mssql` - Microsoft SQL Server database client](https://www.npmjs.com/package/mssql)

## Java

* Java SE
* Java EE
* JNLP
* Servlet
* Spring Framework / Spring Boot
* Hibernate ORM
* JavaServer Faces (JSF)
* JBoss RichFaces
* Enterprise Java Beans (EJB)
* Jetty
* **Web server**: Apache Tomcat, JBoss, Glassfish
* **IDE**: Eclipse, NetBeans, IntelliJ IDEA

## Best practices

* [Twelve Factor App](https://12factor.net/)

## Misc

### Commands

* Terminal: `tty`, `tmux`
* OS information: `uname`, `lsb_release`, `lscpu`, `screenfetch`
* Process management: `ps`, `kill`, `watch`
* Daemon: `systemctl`, `sysctl`, `service`
* Directory navigation: `pwd`, `pushd`, `popd`
* Stream handling: `echo`, `tail`, `tee`
* Networking: `netstat`, `ifconfig`
* HTTP client: `wget`, `curl`
* SSH: `ssh`, `openssh-server`, `openssh-client`
* FTP: `ftp`, `sftp`
* SSL: `ssl`, `openssl`

### Libraries

* *Apache Xerces*: Deals with XML, for Java / C++ / Perl
* *Google Protobuf*: Binary wire data, alternative to XML and JSON

### Links

* [Git tutorial by Atlassian](https://www.atlassian.com/git/tutorials/what-is-version-control/)
* [Introduction to load-balancing](https://www.digitalocean.com/community/tutorials/an-introduction-to-haproxy-and-load-balancing-concepts)
* [OpenSSH Server configuration](https://www.ssh.com/ssh/sshd_config/)
* [Explanation on Linux boot run state](https://www.networkworld.com/article/2693438/operating-systems/unix-how-to-the-linux-etc-inittab-file.html)
* [Using Java classes in JavaScript](https://docs.oracle.com/javase/8/docs/technotes/guides/scripting/prog_guide/javascript.html#A1147187)
* [Virtual printer to PDF](http://www.dopdf.com/)
