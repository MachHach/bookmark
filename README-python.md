# Bookmark: Python

## Virtual environment

Initialize a new Python 3 virtual environment with `venv` module, in relative directory `venv`.

```bash
# Execute in Linux
python3 -m venv {venv}
```

```bat
REM Execute in Windows cmd
py -3 -m venv {venv}
```

Activate the Python virtual environment located in relative directory `venv`.

```bash
# Execute in Linux
source venv/bin/activate
```

```bat
REM Execute in Windows cmd
./venv/Scripts/activate
```

Deactivate virtual environment.

```bash
deactivate
```

## PIP package manager

Install all Python packages with corresponding versions specified in a local requirements file.

```bash
pip install -r {requirements.txt} [-r {requirements2.txt}]
```

Install a Python package from PIP repository.

- Specify **-U** to upgrade existing package to latest version.

```bash
pip install [-U] {package-name}
```

Install a Python package from Git repository.

```bash
pip install [-U] git+{repo-url}[@{branch}]
```

Uninstall a Python package.

```bash
pip uninstall [-y] {package-name}
```

Store (and overwrite) list of currently installed Python packages into a local requirements file.

```bash
pip freeze > {requirements.txt}
```

Show an installed Python packages with its upstream and downstream dependencies.

```bash
pip show {package-name}
```

### Standard libraries

* [Built-in constants](https://docs.python.org/3/library/constants.html)
* [Built-in exceptions](https://docs.python.org/3/library/exceptions.html)
* [Built-in functions](https://docs.python.org/3/library/functions.html)
* [Built-in types](https://docs.python.org/3/library/stdtypes.html)

* [`abc` - Abstract class functions](https://docs.python.org/3/library/abc.html)
* [`argparse` - CLI argument parsing utlity](https://docs.python.org/3/library/argparse.html)
* [`cmd` - Interactive CLI](https://docs.python.org/3/library/cmd.html)
* [`collections` - Special collection classes](https://docs.python.org/3/library/collections.html)
* [`collections.abc` - Abstract collection classes](https://docs.python.org/3/library/collections.abc.html)
* [`configparser` - INI parser](https://docs.python.org/3/library/configparser.html)
* [`contextlib` - Context manager utilities](https://docs.python.org/3/library/contextlib.html)
* [`copy` - Cloning objects](https://docs.python.org/3/library/copy.html)
* [`csv` - CSV parser](https://docs.python.org/3/library/csv.html)
* [`datetime` - Date time & time zone](https://docs.python.org/3/library/datetime.html)
* [`enum` - Enumeration class](https://docs.python.org/3/library/enum.html)
* [`ftplib` - FTP client](https://docs.python.org/3/library/ftplib.html)
* [`functools` - Functional programming utilities](https://docs.python.org/3/library/functools.html)
* [`getpass` - Prompt user for password](https://docs.python.org/3/library/getpass.html)
* [`hashlib` - Hashing utilities](https://docs.python.org/3/library/hashlib.html)
* [`html` - HTML data utilities](https://docs.python.org/3/library/html.html)
* [`html.parser` - HTML parser](https://docs.python.org/3/library/html.parser.html)
* [`http` - Working with HTTP](https://docs.python.org/3/library/http.html)
* [`io` - I/O stream utilities](https://docs.python.org/3/library/io.html)
* [`ipaddress` - IPv4/IPv6 address utilities](https://docs.python.org/3/library/ipaddress.html)
* [`inspect` - Object metadata inspector](https://docs.python.org/3/library/inspect.html)
* [`itertools` - Iterator utilities](https://docs.python.org/3/library/itertools.html)
* [`json` - JSON parser](https://docs.python.org/3/library/json.html)
* [`logging` - Logging utilities](https://docs.python.org/3/library/logging.html)
* [`logging.config` - Logging configuration](https://docs.python.org/3/library/logging.config.html)
* [`logging.handlers` - Logging handlers](https://docs.python.org/3/library/logging.handlers.html)
* [`math` - Math functions](https://docs.python.org/3/library/math.html)
* [`operator` - Operator as function](https://docs.python.org/3/library/operator.html)
* [`os` - OS-dependent functions](https://docs.python.org/3/library/os.html)
* [`os.environ` - Get environment variables](https://docs.python.org/3/library/os.html#os.environ)
* [`os.path` - File paths](https://docs.python.org/3/library/os.path.html)
* [`pathlib` - High-level file paths](https://docs.python.org/3/library/pathlib.html)
* [`platform` - Platform information](https://docs.python.org/3/library/platform.html)
* [`pprint` - Pretty printer](https://docs.python.org/3/library/pprint.html)
* [`queue` - Synchronized queue](https://docs.python.org/3/library/queue.html)
* [`random` - Random value generator](https://docs.python.org/3/library/random.html)
* [`re` - Regular expression](https://docs.python.org/3/library/re.html)
* [`secrets` - Secure random value generator](https://docs.python.org/3/library/secrets.html)
* [`shutil` - High-level file operation](https://docs.python.org/3/library/shutil.html)
* [`signal` - OS process signals](https://docs.python.org/3/library/signal.html)
* [`socket` - TCP/UDP socket](https://docs.python.org/3/library/socket.html)
* [`socket.getfqdn` - Get fully-qualified domain name](https://docs.python.org/3/library/socket.html#socket.getfqdn)
* [`ssl` - SSL/TLS utilities](https://docs.python.org/3/library/ssl.html)
* [`string` - String operation](https://docs.python.org/3/library/string.html)
* [`subprocess` - Start/stop/monitor child OS processes](https://docs.python.org/3/library/subprocess.html)
* [`sys` - Controlling the Python intepreter](https://docs.python.org/3/library/sys.html)
* [`tarfile` - TAR archive](https://docs.python.org/3/library/tarfile.html)
* [`tempfile` - Temporary file / directory](https://docs.python.org/3/library/tempfile.html)
* [`threading` - OS thread utilities](https://docs.python.org/3/library/threading.html)
* [`time` - Process-related time functions](https://docs.python.org/3/library/time.html)
* [`traceback` - Stack trace utilities](https://docs.python.org/3/library/traceback.html)
* [`typing` - Type hinting](https://docs.python.org/3/library/typing.html)
* [`unittest` - Unit test utilities](https://docs.python.org/3/library/unittest.html)
* [`unittest.mock` - Unit test object mocking utilities](https://docs.python.org/3/library/unittest.mock.html)
* [`urllib.parse` - URL parser](https://docs.python.org/3/library/urllib.parse.html)
* [`urllib.robotparser` - robots.txt parser](https://docs.python.org/3/library/urllib.robotparser.html)
* [`uuid` - UUID generator](https://docs.python.org/3/library/uuid.html)
* [`webbrowser` - Web browser controller](https://docs.python.org/3/library/webbrowser.html)
* [`winreg` - Windows registry utilities](https://docs.python.org/3/library/winreg.html)
* [`zipfile` - ZIP archive](https://docs.python.org/3/library/zipfile.html)

* [`base64` - Base64 encoder](https://docs.python.org/3/library/base64.html)
* [`concurrent.futures` - Parallel executor for functions](https://docs.python.org/3/library/concurrent.futures.html)
* [`curses` - Terminal display handling](https://docs.python.org/3/library/curses.html)
* [`email` - Email data handling](https://docs.python.org/3/library/email.html)
* [`getopt` - Low-level CLI argument parsing](https://docs.python.org/3/library/getopt.html)
* [`glob` - Unix pathname search](https://docs.python.org/3/library/glob.html)
* [`hmac` - Hashing with HMAC algorithm](https://docs.python.org/3/library/hmac.html)
* [`mimetypes` - MIME type detector](https://docs.python.org/3/library/mimetypes.html)
* [`multiprocessing` - Multi-process concurrency](https://docs.python.org/3/library/multiprocessing.html)
* [`netrc` - Working with netrc](https://docs.python.org/3/library/netrc.html)
* [`sched` - Event scheduler for functions](https://docs.python.org/3/library/sched.html)
* [`telnetlib` - Telnet client](https://docs.python.org/3/library/telnetlib.html)
* [`urllib.request` - High-level wrapper for `http.client`](https://docs.python.org/3/library/urllib.request.html)

### PIP packages

* [`click` - Highly-composable CLI](https://pypi.org/project/click/)
* [`defusedxml` - Secure XML parser](https://pypi.org/project/defusedxml/)
* [`Flask` - Simple web framework](https://pypi.org/project/Flask/)
* [`lxml` - XML library](https://pypi.org/project/lxml/)
* [`psutil` - System utilities](https://pypi.org/project/psutil/)
* [`pypiwin32` - Win32 API](https://pypi.org/project/pypiwin32/)
* [`python-dotenv` - Environment variables as file](https://pypi.org/project/python-dotenv/)
* [`requests` - HTTP client library](https://pypi.org/project/requests/)
* [`ruamel.yaml` - YAML parser](https://pypi.org/project/ruamel.yaml/)
* [`Twisted` - Event-driven networking](https://pypi.org/project/Twisted/)

* [`pytest` - Unit test framework](https://pypi.org/project/pytest/)
* [`Sphinx` - Documentation generator](https://pypi.org/project/Sphinx/)
* [`tox` - Testing against different Python interpreters](https://pypi.org/project/tox/)

### Python guides

* [How to configure via environment variables](https://opensourcehacker.com/2012/12/13/configuring-your-python-application-using-environment-variables/)
